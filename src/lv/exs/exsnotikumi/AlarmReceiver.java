package lv.exs.exsnotikumi;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

@SuppressLint("SimpleDateFormat")
public class AlarmReceiver extends BroadcastReceiver {
	String[] from = new String[] { "name", "time" };
	ArrayList<String> dateArray = new ArrayList<String>();
	List<HashMap<String, Object>> fillMaps = new ArrayList<HashMap<String, Object>>();
	HashMap<String, Object> map;
	FileInputStream fIn;
	Context mainContext = null;
	SharedPreferences appPrefs;
	Editor edit;
	NotificationManager notificationManager;
	int connectionInterrupted = 0;
	int newEvents;
	String lastSeenString;
	long diffMinutes = 30;
	String userID;

	@Override
	public void onReceive(Context context, Intent inte) {
		mainContext = context;
		appPrefs = context.getSharedPreferences(
				"lv.exs.exsnotikumi_preferences", Context.MODE_PRIVATE);
		userID = appPrefs.getString("userID", "0");
		if (isNetworkAvailable()
				&& appPrefs.getBoolean("automaticallyUpdate", true) == true) {
			connectionInterrupted = 0;
			new ReadBackgroundJSONFeedTaskDate().execute("http://exs.lv/user_stats/json/" + userID + "/");
			edit = appPrefs.edit();
			notificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
		} else {
			// Log.d("AlarmJava", "NO NETWORK");
			Log.d("AlarmJava", "autoUpdate = false || no network");
		}
	}	
	
	private String readJSONFeed(String URL) {
		StringBuilder stringBuilder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URL);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
				}
			} else {
				Log.e("JSON", "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}

	public class ReadBackgroundJSONFeedTask extends
			AsyncTask<String, Void, String> {
		protected String doInBackground(String... urls) {
			return readJSONFeed(urls[0]);
		}

		protected void onPostExecute(String result) {
			try {
				JSONArray jsonArray = new JSONArray(result);
				Log.i("JSON", "Number of events: " + jsonArray.length());
				// call fillMaps.clear so that listview does not multiply, but a
				// fresh
				// one starts instead
				fillMaps.clear();
				dateArray.clear();
				for (int i = 0; i < jsonArray.length(); i++) {
					if (isNetworkAvailable()) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						// events[i]=jsonObject.getString("title");
						map = new HashMap<String, Object>();
						map.put("name", jsonObject.getString("title"));
						map.put("time", jsonObject.getString("date"));
						dateArray.add(jsonObject.getString("date") + " "
								+ jsonObject.getString("type"));

						if (jsonObject.getString("type").equals("0")
								|| jsonObject.getString("type").equals("8")) {
							map.put("picID",
									Integer.toString(R.drawable.atbilde_komentars_t0_t8));
						} else if (jsonObject.getString("type").equals("1")) {
							map.put("picID", Integer
									.toString(R.drawable.komentars_galerija_t1));
						} else if (jsonObject.getString("type").equals("2")) {
							map.put("picID", Integer
									.toString(R.drawable.komentars_raksts_t2));
						} else if (jsonObject.getString("type").equals("3")) {
							map.put("picID", Integer
									.toString(R.drawable.atbilde_miniblogs_t3));
						} else if (jsonObject.getString("type").equals("4")) {
							map.put("picID", Integer
									.toString(R.drawable.lietotajs_grupa_t4));
						} else if (jsonObject.getString("type").equals("5")) {
							map.put("picID", Integer
									.toString(R.drawable.aicina_draudzeties_t5));
						} else if (jsonObject.getString("type").equals("6")) {
							map.put("picID",
									Integer.toString(R.drawable.aicinajums_apstiprinats_t6));
						} else if (jsonObject.getString("type").equals("7")) {
							map.put("picID", Integer
									.toString(R.drawable.jauna_medala_t7));
						} else if (jsonObject.getString("type").equals("9")) {
							map.put("picID", Integer
									.toString(R.drawable.jauna_vestule_t9));
						} else if (jsonObject.getString("type").equals("10")) {
							map.put("picID",
									Integer.toString(R.drawable.warn_add_t10));
						} else if (jsonObject.getString("type").equals("11")) {
							map.put("picID", Integer
									.toString(R.drawable.warn_remove_t11));
						} else if (jsonObject.getString("type").equals("12")) {
							map.put("picID",
									Integer.toString(R.drawable.exs_news_t12));
						} else if (jsonObject.getString("type").equals("13")
								|| jsonObject.getString("type").equals("14")
								|| jsonObject.getString("type").equals("15")
								|| jsonObject.getString("type").equals("16")) {
							map.put("picID",
									Integer.toString(R.drawable.mention_t13_t14_t15_t16));
						}
						String infostr = "";
						String subInfo = "";
						infostr = (jsonObject.getString("info"));
						if (infostr.length() > 0) {
							infostr = infostr.replaceAll("&quot;", "\"");
							if (infostr.toString().length() > 19) {
								subInfo = infostr.toString().substring(0, 19);
								subInfo = subInfo + "..";
							} else {
								subInfo = infostr.toString();
							}
						}
						map.put("info", subInfo.toString());

						fillMaps.add(map);
						// Log.d("AlarmJava", "Getting json!");
						} else {
							connectionInterrupted = 1;
							Log.d("AlarmJava", "Connection interrupted!");
							break;
						}
					}
			} catch (Exception e) {
				e.printStackTrace();
			}
			// only complete checking if auto-update completed perfectly
			if (((appPrefs.getString("cachedEvents", "")).equals(fillMaps
					.toString()))
					|| (connectionInterrupted == 1)
					|| fillMaps.isEmpty()) {
				Log.d("AlarmJava", "No new events");
			} else {
				Log.d("AlarmJava", "NEW EVENTS!");

				int eventID = 0;
				if (!dateArray.isEmpty()) {
					String arrayString = appPrefs
							.getString("cachedDates", null);
					if (appPrefs.getString("cachedDates", null) != null) {
						arrayString = arrayString.substring(1);
						arrayString = arrayString.substring(0,
								arrayString.length() - 1);
						arrayString = arrayString.replaceAll("  ", "");
						List<String> tempList = Arrays.asList(arrayString
								.split(","));
						ArrayList<String> cachedDates = new ArrayList<String>();

						for (int x = 0; x < tempList.size(); x++) {
							cachedDates.add(tempList.get(x).trim());
						}
						// events that disappear from list
						int eventCounter = 0;
						while (cachedDates.indexOf(dateArray.get(eventCounter)) == -1) {
							eventCounter++;
						}
						Log.d("AlarmJava", "New events: " + eventCounter);

						if (eventCounter != 0) {
							newEvents = eventCounter;
							for (int x = 0; x < eventCounter; x++) {
								if (dateArray.get(x).length() == 21) {
									// substring 20, 21 is the type
									eventID = Integer.parseInt(dateArray.get(x)
											.substring(20, 21));
									if (doNotify(eventID)) {
										notifyFunc();
										break;
									} else {
										// do nothing
									}
								} else {
									// substring 20, 22 is the type
									eventID = Integer.parseInt(dateArray.get(x)
											.substring(20, 22));
									if (doNotify(eventID)) {
										notifyFunc();
										Log.d("AlarmJava",
												"Received a new id and pushed notification");
										break;
									} else {
										Log.d("AlarmJava",
												"Received a new id, but did not push");
									}

								}
							}
						}

					}
					edit.putString("cachedDates", dateArray.toString());
				}

				// Log.d("AlarmJava",
				// "cached: "+appPrefs.getString("cachedEvents", ""));
				// Log.d("AlarmJava", "new: "+fillMaps.toString());
				edit.putString("cachedEvents", fillMaps.toString());
				edit.commit();
			}
		}
	}

	private boolean doNotify(int eventId) {
		return appPrefs.getBoolean("type" + eventId, true);
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) mainContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	private void notifyFunc() {
		if (appPrefs.getBoolean("noUpdatingWhileActive", false) == true) {
			if (diffMinutes > 29) {
				Intent intent = new Intent(mainContext, Main.class);
				Notification notification = null;
				if (newEvents == 1) {
					notification = new Notification(R.drawable.ic_launcher,
							"Jauns notikums @exs.lv",
							System.currentTimeMillis());
				} else {
					notification = new Notification(R.drawable.ic_launcher,
							newEvents + " jauni notikumi @exs.lv",
							System.currentTimeMillis());
				}
				intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_CLEAR_TOP);
				PendingIntent pIntent = PendingIntent.getActivity(mainContext,
						0, intent, 0);
				notification.contentIntent = pIntent;
				notification.flags |= Notification.FLAG_AUTO_CANCEL;
				if (appPrefs.getBoolean("doSound", true)) {
					notification.defaults |= Notification.DEFAULT_SOUND;
				}
				if (appPrefs.getBoolean("doVibrate", true)) {
					notification.defaults |= Notification.DEFAULT_VIBRATE;
				}
				if (newEvents == 1) {
					notification.setLatestEventInfo(mainContext,
							"eXs notikumi", "Jauns notikums @exs.lv!", pIntent);
				} else {
					notification.setLatestEventInfo(mainContext,
							"eXs notikumi", newEvents
									+ " jauni notikumi @exs.lv!", pIntent);
				}
				notificationManager.notify(0, notification);
			}
		}
		else{
			Intent intent = new Intent(mainContext, Main.class);
			Notification notification = null;
			if (newEvents == 1) {
				notification = new Notification(R.drawable.ic_launcher,
						"Jauns notikums @exs.lv",
						System.currentTimeMillis());
			} else {
				notification = new Notification(R.drawable.ic_launcher,
						newEvents + " jauni notikumi @exs.lv",
						System.currentTimeMillis());
			}
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_CLEAR_TOP);
			PendingIntent pIntent = PendingIntent.getActivity(mainContext,
					0, intent, 0);
			notification.contentIntent = pIntent;
			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			if (appPrefs.getBoolean("doSound", true)) {
				notification.defaults |= Notification.DEFAULT_SOUND;
			}
			if (appPrefs.getBoolean("doVibrate", true)) {
				notification.defaults |= Notification.DEFAULT_VIBRATE;
			}
			if (newEvents == 1) {
				notification.setLatestEventInfo(mainContext,
						"eXs notikumi", "Jauns notikums @exs.lv!", pIntent);
			} else {
				notification.setLatestEventInfo(mainContext,
						"eXs notikumi", newEvents
								+ " jauni notikumi @exs.lv!", pIntent);
			}
			notificationManager.notify(0, notification);
		}
	}
	
	public class ReadBackgroundJSONFeedTaskDate extends
			AsyncTask<String, Void, String> {
		protected String doInBackground(String... urls) {
			return readJSONFeed(urls[0]);
		}

		protected void onPostExecute(String result) {
			try {				
				JSONObject jsonObject = new JSONObject(result);
					if (isNetworkAvailable()) {
						Log.d("AlarmJava","Getting last seen date...2");
						lastSeenString=jsonObject.getString("last_seen");

						lastSeenString = lastSeenString.replaceAll("-", "/");
						Log.d("AlarmJava","LAST SEEN: "+lastSeenString);
						
						
						String dateStart = lastSeenString;
						Calendar c = Calendar.getInstance();

						int month = c.get(Calendar.MONTH)+1;
						String dateStop = c.get(Calendar.YEAR) + "/" 
						+ month
						+ "/" + c.get(Calendar.DAY_OF_MONTH) 
						+ " " + c.get(Calendar.HOUR_OF_DAY) 
						+ ":" + c.get(Calendar.MINUTE)
						+ ":" + c.get(Calendar.SECOND);
						
						Log.d("AlarmJava","dateStop: "+dateStop);
				 
						//HH converts hour in 24 hours format (0-23), day calculation
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
						
						Date d1 = null;
						Date d2 = null;
						
						try {
							d1 = format.parse(dateStart);
							d2 = format.parse(dateStop);
							//in milliseconds
							long diff = d2.getTime() - d1.getTime();
							diffMinutes = diff / (60 * 1000);
							Log.d("AlarmJava","Minutes passed =  "+ diffMinutes);
				 
						} catch (Exception e) {
							e.printStackTrace();
						}
						new ReadBackgroundJSONFeedTask()
						.execute("http://exs.lv/notifications/json/" + userID + "/");
						
					} else {
						Log.d("AlarmJava", "Connection interrupted!");
					}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}