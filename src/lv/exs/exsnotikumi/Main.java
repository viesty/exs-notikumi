package lv.exs.exsnotikumi;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.content.SharedPreferences;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class Main extends Activity implements AdapterView.OnItemClickListener {
	String[] from = new String[] { "name", "time", "picID", "info" };
	ArrayList<String> dateArray = new ArrayList<String>();
	int[] to = new int[] { R.id.eventName, R.id.eventTime, R.id.picID,
			R.id.eventInfo };
	List<HashMap<String, Object>> fillMaps = new ArrayList<HashMap<String, Object>>();
	HashMap<String, Object> map;
	SharedPreferences appPrefs;
	Editor edit;
	public Context context;
	String cachedEvents = "";
	String[] links = new String[20];
	String substr;
	String timeToUpdateString;
	int timeToUpdate;
	ListView list;
	ProgressBar progressBar;
	ViewGroup root;
	Boolean canAddBar = true;
	Intent alarmIntent;
	AlarmManager alarmManager;
	PendingIntent pendingIntent;

	public String readJSONFeed(String URL) {
		StringBuilder stringBuilder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URL);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
				}
			} else {
				Log.e("JSON", "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}

	private class ReadJSONFeedTask extends AsyncTask<String, Void, String> {
		protected String doInBackground(String... urls) {
			return readJSONFeed(urls[0]);
		}

		@SuppressLint("NewApi")
		protected void onPostExecute(String result) {
			try {
				JSONArray jsonArray = new JSONArray(result);
				Log.i("JSON", "Number of events: " + jsonArray.length());
				// call fillMaps.clear so that listview does not multiply, but a
				// fresh
				// one starts instead
				fillMaps.clear();
				dateArray.clear();
				/*
				 * 0 - atbilde komentaram 1 - komentars bildei 2 - komentars
				 * rakstam 3 - komentars minibloga 4 - lietotajs grupaa 5 -
				 * uzaicinaja draudzeties 6 - apstiprin�ja draudz�bas
				 * aizcin�jumu 7 - jauna meda�a 8 - atbilde grup� 9 - jauna
				 * v�stule 10 - warns pielikts 11 - warns no�emts 12 - exs.lv
				 * update 13 - @mention grup� 14 - @mention miniblog� 15 -
				 * 
				 * @mention topik� 16 - @mention att�la komentos
				 */
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObject = jsonArray.getJSONObject(i);
					links[i] = jsonObject.getString("url");
					map = new HashMap<String, Object>();
					map.put("name", jsonObject.getString("title"));
					map.put("time", jsonObject.getString("date"));
					dateArray.add(jsonObject.getString("date")+" "+jsonObject.getString("type"));
					
					if (jsonObject.getString("type").equals("0")
							|| jsonObject.getString("type").equals("8")) {
						map.put("picID", Integer
								.toString(R.drawable.atbilde_komentars_t0_t8));
					} else if (jsonObject.getString("type").equals("1")) {
						map.put("picID", Integer
								.toString(R.drawable.komentars_galerija_t1));
					} else if (jsonObject.getString("type").equals("2")) {
						map.put("picID", Integer
								.toString(R.drawable.komentars_raksts_t2));
					} else if (jsonObject.getString("type").equals("3")) {
						map.put("picID", Integer
								.toString(R.drawable.atbilde_miniblogs_t3));
					} else if (jsonObject.getString("type").equals("4")) {
						map.put("picID",
								Integer.toString(R.drawable.lietotajs_grupa_t4));
					} else if (jsonObject.getString("type").equals("5")) {
						map.put("picID", Integer
								.toString(R.drawable.aicina_draudzeties_t5));
					} else if (jsonObject.getString("type").equals("6")) {
						map.put("picID",
								Integer.toString(R.drawable.aicinajums_apstiprinats_t6));
					} else if (jsonObject.getString("type").equals("7")) {
						map.put("picID",
								Integer.toString(R.drawable.jauna_medala_t7));
					} else if (jsonObject.getString("type").equals("9")) {
						map.put("picID",
								Integer.toString(R.drawable.jauna_vestule_t9));
					} else if (jsonObject.getString("type").equals("10")) {
						map.put("picID",
								Integer.toString(R.drawable.warn_add_t10));
					} else if (jsonObject.getString("type").equals("11")) {
						map.put("picID",
								Integer.toString(R.drawable.warn_remove_t11));
					} else if (jsonObject.getString("type").equals("12")) {
						map.put("picID",
								Integer.toString(R.drawable.exs_news_t12));
					} else if (jsonObject.getString("type").equals("13")
							|| jsonObject.getString("type").equals("14")
							|| jsonObject.getString("type").equals("15")
							|| jsonObject.getString("type").equals("16")) {
						map.put("picID", Integer
								.toString(R.drawable.mention_t13_t14_t15_t16));
					}
					String infostr = "";
					String subInfo = "";
					infostr = (jsonObject.getString("info"));
					if (infostr.length() > 0) {
						infostr = infostr.replaceAll("&quot;", "\"");
						if (infostr.toString().length() > 19) {
							subInfo = infostr.toString().substring(0, 19);
							subInfo = subInfo + "..";
						} else {
							subInfo = infostr.toString();
						}
					}
					map.put("info", subInfo.toString());					
					fillMaps.add(map);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			// called after json is finished parsing
			//add cached dates+id for accurate notifications
			edit.putString("cachedDates", dateArray.toString());
			//add the cached events to detect if new events exist
			edit.putString("cachedEvents", fillMaps.toString());
			edit.commit();
			/*THIS IS A CHECKING OF TYPE BASED ON THE NUMBER OF DIGITS
			if(appPrefs.getString("cachedDates", "").substring(21, 23).contains(",")){
				Log.d("MainJava","cachedDates(1) type: "+appPrefs.getString("cachedDates", "").substring(21, 22));
			}
			else{
				Log.d("MainJava","cachedDates(1) type: "+appPrefs.getString("cachedDates", "").substring(21, 23));
			}*/
			showlist();
		}

		@SuppressLint("WorldReadableFiles")
		private void showlist() {
			root.removeView(progressBar);
			canAddBar = true;
			list = (ListView) findViewById(R.id.list);
			// We extend activity, not listactivity, so we have to set up a
			// listener
			// by ourselves
			list.setOnItemClickListener(Main.this);
			SimpleAdapter adapter = new SimpleAdapter(Main.this, fillMaps,
					R.layout.list_row, from, to);
			list.setAdapter(adapter);

			Toast.makeText(Main.this, "Atjaunots", Toast.LENGTH_SHORT).show();
			// Log.d("s", "THE MAP: " + fillMaps);
			edit.putString("cachedEvents", fillMaps.toString());
			edit.commit();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		appPrefs = getSharedPreferences("lv.exs.exsnotikumi_preferences",
				MODE_PRIVATE);
		setContentView(R.layout.main);
		if (appPrefs.getBoolean("firstRun", true) == true) {
			startActivity(new Intent(Main.this, Preferences.class));
			Log.d("MainJava", "" + appPrefs.getBoolean("firstRun", true));
		}
		Button showSettings = (Button) findViewById(R.id.btnSettings);
		Button refreshContent = (Button) findViewById(R.id.btnRefresh);
		Button infoButton = (Button) findViewById(R.id.btnInfo);
		edit = appPrefs.edit();
		alarmIntent = new Intent(this, AlarmReceiver.class);
		pendingIntent = PendingIntent.getBroadcast(
				this.getApplicationContext(), 234324743, alarmIntent, 0);
		alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		// fire up every timeToUpdate shared preference
		timeToUpdateString = appPrefs.getString("timeToUpdate", "15");
		timeToUpdate = (Integer.parseInt(timeToUpdateString.toString())) * 60 * 1000;
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis() + timeToUpdate, timeToUpdate,
				pendingIntent);

		// TODO create a saved version of events so you can view it w/o
		// Internet

		if (isNetworkAvailable()) {
			refreshEvents();
		} else {
			Toast.makeText(Main.this, "Nepiecie�ams interneta savienojums",
					Toast.LENGTH_LONG).show();
		}
		showSettings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(Main.this, Preferences.class));
			}
		});
		refreshContent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				refreshEvents();
				timeToUpdateString = appPrefs.getString("timeToUpdate", "15");
				timeToUpdate = (Integer.parseInt(timeToUpdateString.toString())) * 60 * 1000;
				alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
						System.currentTimeMillis() + timeToUpdate,
						timeToUpdate, pendingIntent);
			}
		});
		infoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog alertDialog = new AlertDialog.Builder(Main.this)
						.create();

				// Setting Dialog Title
				alertDialog.setTitle("Inform�cija");

				// Setting Dialog Message
				alertDialog
						.setMessage("Sveiks un paldies, ka lieto �o aplik�ciju!\n"
								+ "1. Datu pat�ri�� \n"
								+ "Neap�aub�mi, �� aplik�cija savai darb�bai izmanto interneta "
								+ "savienojumu. Apr��in�t prec�zo pat�ri�u ir sam�r� sare���ti, "
								+ "bet aptuvenie r�d�t�ji ir ��di: \n"
								+ "   -> Viena atjauno�anas reize pat�r� ~4.5KB datu,\n"
								+ "   -> Atjaunojot sarakstu ik p�c 15 min�t�m, ar past�v�gu "
								+ "interneta savienojumu, diennakt� tiks pat�r�ti ~432KB datu.\n"
								+ "\n"
								+ "2. 'Man nestr�d� pazi�ojumi!'\n"
								+ "Pirmk�rt, p�rbaudi, vai uzst�d�jumos tev ir iesp�jota autom�tisk� "
								+ "atjauno�ana, kas ar� atbild par pazi�ojumu nodo�anu.\n"
								+ "Otrk�rt, ��da k��me var�tu rasties, ja tu izmanto k�du aplik�ciju "
								+ "apst�din��anas automatiz�ciju, piem�ram, TaskKiller. \n"
								+ "Tre�k�rt, pazi�ojumi netiks par�d�ti un autom�tisk� p�rbaude "
								+ "nenotiks, ja aplik�cijai tiks veikta piespiedu aizv�r�ana.\n"
								+ "\n"
								+ "3. 'Man trauc� ska�as efekti un vibr�cija!' \n"
								+ "Tas viss ir main�ms uzst�d�jumos.\n"
								+ "\n"
								+ "4. Baterijas pat�ri��\n"
								+ "To noteikt ir v�l gr�t�k k� datu pat�ri�u, t�p�c, ka ir p�r�k "
								+ "daudz ietekm�jo�o faktoru. Tom�r aptuvenie apr��ini nor�da uz "
								+ "sekojo�o -> Atjaunojot sarakstu 15 reizes stund� pie stabila wifi "
								+ "savienojuma, tiks pat�r�ts ~1% baterijas jaudas. Ta�u, j��em v�r�, "
								+ "ka �� aplik�cija noteikti nav vien�gais pat�r�t�js stundas laik�.\n"
								+ "\n"
								+ "Pie aplik�cijas esmu st��d�jis sam�r� ilgi un esmu ar� izm��in�jis "
								+ "vair�kus k��du scen�rijus, ta�u k��d�s jebkur� un neviena "
								+ "aplik�cija nav perfekta, t�p�c "
								+ "jebkuru k��du gad�jum� l�gums sazin�ties ar mani caur exs.lv, "
								+ "vai ar� s�tot e-pastu uz viesty[aet]exs[dot]lv\n"
								+ "\n"
								+ "Aplik�cija neb�tu tapusi bez exs.lv un @mad.\n"
								+ "Aplik�cijas kods: @Viesty - Viesturs Ru��ns.\n"
								+ "Aplik�cijas ikona: @Lord_Eddward.\n"
								+ "Test��ana un atsauksmes/ierosin�jumi veido�anas laik�: @Wanted\n"
								+ "\n"
								+ "Atjaunin�jumu v�sture: \n"
								+ "------\n"
								+ "v1.4 - 11.11.2013\n"
								+ "=> Nezi�ot, kam�r esi akt�vs - Vairs aplik�cija "
								+ "tevi nekaitin�s, ja par visu to, ko redzi @ exs.lv "
								+ "tiks ar� pazi�ots telefon�. Par jauniem notikumiem "
								+ "tiks pazi�iots tikai t�d� gad�jum�, ja tu p�d�jo "
								+ "30 min��u laik� neb�si man�ts @ exs.lv (tas, protams "
								+ "ir p�c pa�a izv�les un ir uzst�d�ms iestat�jumos)\n"
								+ "=> Neliela koda optimiz�cija; lai ar� neman�mi lietot�jam, ta�u "
								+ "samazin�s baterijas pat�ri�u un izpildes laiku.\n"
								+ "------\n"
								+ "v1.3 - 03.11.2013\n"
								+ "=> Jauna aplik�cijas ikona, pateicoties @Lord_Eddward\n"
								+ "=> Pazi�ojumu filtrs - iesp�ja izv�l�ties, par k�diem notikumiem r�d�t pazi�ojumus, lai "
								+ "netrauc�, piem�ram, katrs draudz�bas uzaicin�jums\n"
								+ "=> Pazi�ojum� tiek par�d�ts jauno notikumu skaits.\n"
								+ "------\n"
								+ "v1.2 - 14.10.2013\n"
								+ "=> K��das labojums - notifik�cija vairs nepar�d�s, "
								+ "paz�dot interneta savienojumam autom�tisk�s atjauno�anas laik�\n"
								+ "=> K��das labojums - netika par�d�ta papildus inform�cija "
								+ "un visi sekojo�ie notikumi, ja tekst� bija maz�k nek� 19 simboli.\n"
								+ "------\n"
								+ "v1.1 - 08.10.2013\n"
								+ "=> No�emts aplik�cijas nosaukuma lauks no t�s loga\n"
								+ "=> Koda optimiz�cija ar ProGuard\n"
								+ "=> Atstarpe no kreis�s malas notikumu ikon�m\n"
								+ "=> Da�iem notikumiem par�d�ta papildinform�cija "
								+ "kreisaj� apak��j� st�r�.\n"
								+ "------\n"
								+ "v1.0 - 08.10.2013\n "
								+ "=> Aplik�cijas pirm� publiski pieejam� versija.\n"
								+ "" + "" + "");

				// Setting Icon to Dialog
				alertDialog.setIcon(R.drawable.ic_launcher);

				// Setting OK Button
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// Write your code here to execute after dialog
								// closed

							}
						});

				// Showing Alert Message
				alertDialog.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// refresh events on click
		switch (item.getItemId()) {
		case R.id.refresh:
			refreshEvents();
			timeToUpdateString = appPrefs.getString("timeToUpdate", "15");
			timeToUpdate = (Integer.parseInt(timeToUpdateString.toString())) * 60 * 1000;
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
					System.currentTimeMillis() + timeToUpdate, timeToUpdate,
					pendingIntent);
			break;
		case R.id.preferences:
			startActivity(new Intent(Main.this, Preferences.class));
		}
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		switch (position) {
		case 0:
			substr = links[0].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 1:
			substr = links[1].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 2:
			substr = links[2].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 3:
			substr = links[3].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 4:
			substr = links[4].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 5:
			substr = links[5].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 6:
			substr = links[6].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 7:
			substr = links[7].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 8:
			substr = links[8].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 9:
			substr = links[9].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 10:
			substr = links[10].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 11:
			substr = links[11].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 12:
			substr = links[12].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 13:
			substr = links[13].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 14:
			substr = links[14].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 15:
			substr = links[15].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 16:
			substr = links[16].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 17:
			substr = links[17].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 18:
			substr = links[18].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		case 19:
			substr = links[19].toString().substring(7);
			substr = "http://m." + substr;
			openURL();
			break;
		}
	}

	private void openURL() {
		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(substr)));
	}

	// used to determine if connection is available
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	private void refreshEvents() {
		// Create a progress bar to display while the list loads
		if (canAddBar == true) {
			progressBar = new ProgressBar(Main.this);
			progressBar.setLayoutParams(new LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			progressBar.setIndeterminate(true);
			// getListView().setEmptyView(progressBar);

			// Must add the progress bar to the root of the layout
			root = (ViewGroup) findViewById(android.R.id.content);
			root.addView(progressBar);
			canAddBar = false;
		}
		// Gets userID from shared preferences
		// appPrefs = getSharedPreferences("lv.exs.exsnotikumi_preferences",
		// MODE_PRIVATE);
		String userID = appPrefs.getString("userID", "0");

		// execute json reading
		if (isNetworkAvailable()) {
			new ReadJSONFeedTask().execute("http://exs.lv/notifications/json/"
					+ userID + "/");
		} else {
			Toast.makeText(Main.this, "Nepiecie�ams interneta savienojums",
					Toast.LENGTH_LONG).show();
			root.removeView(progressBar);
		}
	}
}
