package lv.exs.exsnotikumi;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class AutoStart extends BroadcastReceiver {
	Context mainContext = null;
	SharedPreferences appPrefs;
	String timeToUpdateString;
	int timeToUpdate;

	@Override
	public void onReceive(Context context, Intent arg1) {
		mainContext = context;
		appPrefs = mainContext.getSharedPreferences(
				"lv.exs.exsnotikumi_preferences", Context.MODE_PRIVATE);
		timeToUpdateString = appPrefs.getString("timeToUpdate", "15");
		timeToUpdate = (Integer.parseInt(timeToUpdateString.toString())) * 60 * 1000;
		// Fires up the alarm manager on phone boot
		Intent intent = new Intent(mainContext, AlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(
				mainContext.getApplicationContext(), 234324743, intent, 0);
		AlarmManager alarmManager = (AlarmManager) mainContext
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis() + 10000, timeToUpdate,
				pendingIntent);
	}

}
