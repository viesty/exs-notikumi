package lv.exs.exsnotikumi;

import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.Toast;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;

public class Preferences extends PreferenceActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.myapppreferences);
		SharedPreferences appPrefs = getSharedPreferences("lv.exs.exsnotikumi_preferences",
				MODE_PRIVATE);
		Editor edit;
		edit = appPrefs.edit();
		if(appPrefs.getBoolean("firstRun", true) == true){
			edit.putBoolean("firstRun", false);
			edit.commit();
			Log.d("MainJava","a: "+appPrefs.getBoolean("firstRun", true));
		}
		final CheckBoxPreference auto = (CheckBoxPreference)findPreference("automaticallyUpdate");
		final CheckBoxPreference noUpdate = (CheckBoxPreference)findPreference("noUpdatingWhileActive");
		final EditTextPreference time = (EditTextPreference)findPreference("timeToUpdate");
		
		auto.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference arg0) {
				if (auto.isChecked()){
					time.setEnabled(true);
					noUpdate.setEnabled(true);
				}
				else if(auto.isChecked()==false){
					time.setEnabled(false);
					noUpdate.setEnabled(false);
				}
				return false;
			}
		});
		
		time.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				if(Integer.parseInt(newValue.toString())<1){
					Toast.makeText(getApplicationContext(), "Atjauno�anas laikam ir j�b�t vismaz 10 min�t�m", Toast.LENGTH_SHORT).show();
				}
				else if(Integer.parseInt(newValue.toString())>600){
					Toast.makeText(getApplicationContext(), "Atjauno�anas laiks nedr�kst p�rsniegt 600 min�tes", Toast.LENGTH_SHORT).show();
				}
				else{
					Toast.makeText(getApplicationContext(), "Lai apstiprin�tu nomain�to laiku, manu�li atjauno sarakstu vai restart� aplik�ciju", Toast.LENGTH_LONG).show();
					return true;
				}
				return false;
			}
		});
	}
}
