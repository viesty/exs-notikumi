package lv.exs.exsnotikumi;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkChange extends BroadcastReceiver {
	Context context = null;
	SharedPreferences appPrefs;
	String timeToUpdateString;
	int timeToUpdate;

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		// TODO Auto-generated method stub
		context = arg0;

		if (isNetworkAvailable()) {
			Log.d("NetworkChange","Network available!");
			appPrefs = context.getSharedPreferences(
					"lv.exs.exsnotikumi_preferences", Context.MODE_PRIVATE);
			timeToUpdateString = appPrefs.getString("timeToUpdate", "15");
			// TODO: change
			// (Integer.parseInt(timeToUpdateString.toString()))*1000;
			// to (Integer.parseInt(timeToUpdateString.toString()))*60*1000;
			timeToUpdate = (Integer.parseInt(timeToUpdateString.toString())) * 60 * 1000;
			// TODO Auto-generated method stub
			// Fires up the alarm manager on phone boot
			Intent intent = new Intent(context, AlarmReceiver.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(
					context.getApplicationContext(), 234324743, intent, 0);
			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			// fire up every 300,000 ms == 5 minutes
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
					System.currentTimeMillis() + 1000, timeToUpdate,
					pendingIntent);
		}
		else{
			Log.d("NetworkChange","Network NOT available!");
		}
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
